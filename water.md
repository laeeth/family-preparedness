# Notes on Water Preparedness

## Bathing

## Washing (Clothes/Dishes)

## Drinking / Cooking

### Source
  - switch to move borehole to generator
  - hand pump for borehole
  - map out alternative sources and carrying equipment
  - rainwater catching and storage

### Purification
  - bleach drops
  - iodine tablets
  - purification cartridge
  - UV

### Boiling
  - firewood to build fire / boil water twice daily
    - large saw / axe to cut tree
    - identify locations to find firewood
    - how to transport firewood
    - practice splitting firewood
