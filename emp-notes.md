# Grid-Down Notes

## Sources
  - Hellerman: Survival Theory I
  - [Living without electricity](https://raeng.org.uk/media/xrrigg0m/raeng-living-without-electricity.pdf) - a report from the Royal Academy of Engineering on the experience of Lancaster during the 2015 flooding

## Consequences
  - Cyber attack or physical attack on infrastructure
    - Grid down for months and, in some rural locations, years
  - Massive solar storm or small EMP
    - Grid down for six to eighteen months in most locations
  - Super EMP
    - Grid down for years, frying most of the electronics we depend on

EMP (electromagnetic pulse) is the result of detonating a nuclear warhead high above the atmosphere.  Per Compton Effect, as the gamma radiation passes through the upper atmosphere, it ionizes the atoms.  Charged atoms travelling near the speed of light across the affected area (line of sight from the blast) create an electromagnetic pulse.  Rapidly changing current coupled with earth's magnetic field produce damaging current and resulting surge in voltage destroys the grid.

Three types of current produced by an EMP, each with different attributes and affecting electronics differently:
  - E1
  - E2
  - E3

Depends on altitude of detonation, gamma ray output, energy yield and other factors.

In 1962 Starfish Prime - USG exploded nuke 250 mils above Pacific Ocean.  Underestimated results of EMP surge.  Hawaii - 900 miles away - lost 1/3 of streetlights and experienced damages to other electronic equipment.  Infrastructure of that time was robust and repairs easily made.  1/3 of low orbit satellites were crippled or disabled due to unexpected radiation belts formed around earth.  Russia conducted multiple high-altitude nuclear tests over more populated areas with a stronger magnetic field called 'Project K'.  Destroyed large power plant in Karaganda when transformers caught fire.

Sep 1963 Partial Test Ban Treaty signed by US, Russia and England to prohibit nuclear testing in atmosphere or space.  EMP warfare was not very effective against older, hardy electrical infrastructure of that time.  Today where everything has tiny internal circuit boards and microprocessros, EMP pulse would be devastating to infrastructure and way of life.

MAD a deterrent for large state actors, but not for radicalised terrorists.  Agent in Iran military has warned US that Iran is working on an EMP attack.  North Korea has nothing to lose and in 2003 threatened US with nuclear attack.  DHS study found that North Korea already has technology and ability to hit US with an EMP - missile defenses could not stop it.

2004, 2008 Congress Commission - Report of the Commission to Assess the Threat to the United States from EMP.  Country completely vulnerable and most Americans would die within a year.


## Systems vulnerability
  - mobile phone/data coverage loss within hours
  - landlines might still work for days
  - internet mostly lost + no power for domestic routers
  - no electronic payment systems
  - most ATM machines did not work
  - failure for local TV booster + digital audio (DAB) services
  - loss of household lighting and power for appliances
  - loss of electric cooking
  - no power for lifts
  - possible loss of power for water supplies
  - fridge and freezer failure
  - most shops unable to open due to lack of light/refrigeration/electronic tills or card payment systems
  - electric trains affected / signalling problems/ no platform lighting
  - no traffic lights
  - no petrol or diesel sales at garages
  - no TV, internet, text, social media
