# Family Preparedness

This repository is a work-in-progress to share my thoughts on preparing a family to meet certain kinds of challenges in the interesting times ahead.  It's informed by my work as then member of the Executive Committee for a now 300 person / $11bn hedge fund navigating the pandemic of 2020 and serving as informal advisor to policymakers.

## Contingencies to be covered

 - pandemic
 - space weather / EMP / power grid
 - civil disturbances
 - financial collapse

## State of the project

Right now this is just a placeholder - I hope to elaborate as I have time to work on it.


## Reading

 - To do - edit and trim the selection and add brief review comments

### War and Geopolitics
  - War Scare: Russia and America on the Nuclear Brink by Peter Pry
  - The Second Nuclear Age: Strategy, Danger, and the New Power Politics by Paul Bracken
  - On Limited Nuclear War in the 21st Century by Jeffrey Larsen

### Fiction
  - Nuclear War: A Scenario by Annie Jacobsen
  - Lines in the Sand: Post Apocalyptic EMP Survival Fiction by Bobby Akart with intro by Peter Pry
  - EMP Series by Jonathan Hellerman
    - Book 1: Equipping Modern Patriots
    - Book 2: The Aftermath
    - Book 3: New Beginnings
    - Alone: Beth Ann's Story of Survival
  - Limited Exchange by Don Shift

### Biblically-informed 
 - Chaos by Michael Snyder

### EMP / Grid Down
  - Blackout Wars: state initiatives to achieve preparedness against an EMP catastrophe by Peter Pry
  - The Power and The Light by Peter Pry
  - Will America Be Protected?  A Threat Assessment and Progress Report on Implementation of the White House EMP Executive Order by Peter Pry
  - The ABCs of EMP by Jeffrey Yago
  - [Lloyds of London 2013 Report: Solar Storm Risk to the North American Electric Grid](https://assets.lloyds.com/assets/pdf-solar-storm-risk-to-the-north-american-electric-grid/1/pdf-Solar-Storm-Risk-to-the-North-American-Electric-Grid.pdf)

### Nuclear war
  - After the Ashes: Surviving the Coming Nuclear War by Tari Warwick
  - Nuclear War Survival Skills by Cresson Kearny
  - The Doomsday Machine: COnfessions of a Nuclear War Planner by Daniel Ellsberg
  - The Russia Trap by George Beebe
  - Surviving Nuclear War by Stephen Cooper
  - The US Armed Forces Nuclear, Biological and Chemical Survival Manual by Dick Couch
  - Surviving a Nuclear War by George Georgiou
  - War Scare: Russia and America on the Nuclear Brink by Peter Pry

### Preparedness
  - Survival Theory: a preparedness guide - how to survive the end of the world on a budget by Joseph Hellerman
  - Survival Theory II by Joseph Hellerman
  - Suburban Defense by Don Shift

### Society
  - The Fourth Turning is Here by Neil Howe

## Research Sources
  - Jonathan Hellerman, former military Survival, Evasion, Resistance and Escape instructor
    - [Grid Down Consulting](https://www.griddownconsulting.com)
    - [@GridDownPrepper](https://x.com/@GridDownPrepper)

## Shopping Sources
  - [Mormon Church](https://store.lds.org)