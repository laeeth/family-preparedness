# Food Storage

## Quantities required
  - 1,000 to 1,200 calories / day prevents starvation but not enough for much work
  - for heavy, physical work especially in cold weather 3,500 calories/day
  - target 2,000 to 2,500 calories per person per day for duration of emergency

## Calories from food (from Joy of Cooking)
### Rice
  - 1 cup of rice = 200 calories
  - 1 pound raw rice = 6 cups cooked rice = 1,200 cal, 22g protein
### Beans (kidney/navy/pinto)
  - 1 cup cooked = 230 calories
  - 1 pound dried beans = 2 - 2.5 cups = 5 - 6 cups cooked = 1,200 cal, 80g protein
### Oils
  - vegetable oil: 1 tablespoon (1/128 gal) = 110 cal
### Milk
  - skimmed milk reconstituted: 1 cup 82 cal, 8.2gm protein
  


## Water - emergency supplies & treatment
## Wheat, other whole grains, flours and beans
## Powdered milk, dairy products and eggs
## Sweeteners - honey, sugar and syrup
## Cooking catalysts
  - salt, oil and leaveners
## Sprouting seeds and supplies
## Medical care, medications and first aid kits
## Basic supplementation, vitamin, mineral and herbal supplements
## Fuels, energy and camping gear
  - bedding, cooking equipment, other necessities for away from home living
## Personal, family, infant and pet care essentials
## Canned and dried fruits, vegetables and soups
## Kitchen staples - condiments and seasonings
## Masts and seafoods
  - selection of meats - fresh, frozen or Canned
## Domestic maintenance and preparedness
  - items to maintain home, yard, garden
## Pleasure foods - snakcs, beverages, sweets and treats

## Assumptions
- You want to be able to live in a near-normal manner from your own personal resources for up to one year regardless of external conditions.
 - One of your family's four basic goals is to have a one-year, in-home supply of food and nonfood items.
  - You have the means to acquire what you need for your family's in-home food storage.

## Let's begin your in-home storage preparation with these questions:
1. To what potential natural, man-caused, or personal disasters are you vulnerable? How can you eliminate or mitigate their negative impact on your family's life and lifestyle?
2. What if there were no water available from your faucets? · How much drinking water is "hidden" inside and outside your house? ·What are those sources? Could you treat water to make it safe for drinking and cooking?
3. Take an inventory of your refrigerator, freezer, pantry, kitchen cabinets, cupboards, closets, or wherever you put your food. What do you have on hand in these categories:
  - canned foods
  - packaged foods o dried & preserved foods
  - What's there that's nutritious? How long could your family eat if the food in your pantry, refrigerator or freezer were the only food available?
    - 1 day
    - 1 week
    - 1 month
    - longer
4. If you need life-preserving medication, how long will your current supply last if it's no longer available? What vitamin, mineral, and herbal supplements do you have on hand? · In what way do they support your health? · How long would they last if not replenished?
    - 1 day
    - 1 week
    - 1 month
5. Do you know what foods and nonfoods to buy for storage, in which order of priority,  how much of each to buy, and where to buy them?
6. Could you prepare the stored foods, maximizing their shelf life and nutritional qualities?
7. Do you know how to sprout seeds to provide "live" foods for essential vitamins and minerals when there's a shortage of vegetables?
8. Given your current situation, if you could no longer obtain water, food, vitamins, medication and money in a routine manner, how long could you sustain yourself and your family?
The answers to these questions are in the Family Preparedness Handbookl If these questions, or rather, the answers to them, make you uncomfortable, then this is an opportunity for you to start to work on the solutions. If you've prepared for your family's security with emergency and long-term provisions through an in-home storage program, you can turn what might have been a life·threatening situation into a
manageable problem!

[Source](http://www.michiganmilitia.org/html/Can%20You%20Survive.htm)