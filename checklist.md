## Preparedness Test

1. Has your family rehearsed fire escape routes from your home?
1. Does your family know what to do before, during, and after an earthquake or other emergency situation?
1. Do you have heavy objects hanging over beds that can fall during an earthquake?
1. Do you have access to an operational flashlight in every occupied bedroom? (use of candles is not recommended unless you are sure there is no leaking gas)
1. Do you keep shoes near your bed to protect your feet against broken glass?
1. If a water line was ruptured during an earthquake, do you know how to shut off the main water line to your house?
1. Can this water valve be turned off by hand without the use of a tool?  Do you have a tool if one is needed?
1. Do you know where the main gas shut-off valve to your house is located?
1. If you smell gas, do you know how and would you be able to shut off this valve?
1. Gas valves usually cannot be turned off by hand. Is there a tool near your valve?
1. Would you be able to safely restart your furnace when gas is safely available?
1. Do you have working smoke alarms in the proper places to warn you of fire?
1. In case of a minor fire, do you have a fire extinguisher that you know how to use?
1. Do you have duplicate keys and copies of important insurance and other papers stored outside your home?
1. Do you have a functional emergency radio to receive emergency information?
1. If your family had to evacuate your home, have you identified a meeting place?

IF AN EMERGENCY LASTED FOR THREE DAYS ( 72 HOURS) BEFORE HELP WAS AVAILABLE TO YOU AND YOUR FAMILY
1. Would you have sufficient food?
1. Would you have the means to cook food without gas and electricity?
1. Would you have sufficient water for drinking, cooking, and sanitary needs?
1. Do you have access to a 72 hour evacuation kit?
1. Would you be able to carry or transport these kits?
1. Have you established an out-of-state contact?
1. Do you have a first aid kit in your home and in each car?
1. Do you have work gloves and some tools for minor rescue and clean up?
1. Do you have emergency cash on hand? (During emergencies banks and ATMs are closed)
1. Without electricity and gas do you have a way to heat at least part of your house?
1. If you need medications, do you have a month’s supply on hand?
1. Do you have a plan for toilet facilities if there is an extended water shortage?
1. Do you have a supply of food, clothing, and fuel where appropriate?  For 6 months? For a year?

These are all questions that need answers if you are to be safe in an emergency.

If you answered ‘No’ to any of them, its now time to work on getting those items done.

(source - LDS Preparedness Manual Oct 1st 2008 v 5.01)